import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

const IconData thermometer = IconData(0xf491, fontFamily: 'MaterialIcons');
const IconData wind = IconData(0xf89e, fontFamily: 'MaterialIcons');
const IconData compass = IconData(0xf8ca, fontFamily: 'MaterialIcons');

void main() {
  runApp(const WeatherPage());
}

class WeatherPage extends StatefulWidget {
  const WeatherPage({super.key});

  @override
  State<WeatherPage> createState() => _WeatherPage();
}

class _WeatherPage extends State<WeatherPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: buildAppBar(),
        body: buildBackGround(),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // backgroundColor: Colors.transparent,
        // elevation: 0,
        // title: Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceAround,
        // ),
        // actions: <Widget>[
        //   IconButton(
        //       onPressed: () {},
        //       icon: const Icon(Icons.search),
        //       color: Colors.white),
        //   IconButton(
        //       onPressed: () {},
        //       icon: const Icon(Icons.menu),
        //       color: Colors.white),
        // ],
        );
  }

  Widget buildBackGround() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(32),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(
                    "https://scontent.fbkk5-3.fna.fbcdn.net/v/t1.15752-9/320929800_689834899590939_2716260999268737200_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeFW2K2qYBhpUoRBzg_h9w_YX_lktrrlmaZf-WS2uuWZpvvG-R1_BwEM3vPfHTHylLq_2pKssC1lAwfuXezJUcfe&_nc_ohc=zkHnuV2OccIAX8BH1Yy&_nc_ht=scontent.fbkk5-3.fna&oh=03_AdTtFWLMYrR2-Hiao1kY_Ux08SnuWMFJtasaPxrycEVcaA&oe=63DA3145"),
                fit: BoxFit.cover)),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                buildBodyWidget(),
                buildDivider(),
                buildCardWidget(),
                buildDivider(),
                buildExtendBoradcast(),
                buildDivider(),
                buildMoreExtend(),
                buildHelpfulInfo(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMoreExtend() {
    return Row(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 10, right: 170),
          child: Text(
            'HELPFUL INFO',
            style: TextStyle(color: Colors.white),
          ),
        ),
        Text(
          'MORE',
          style: TextStyle(color: Colors.blueGrey),
        ),
        Icon(
          Icons.arrow_right,
          color: Colors.blueGrey,
        ),
      ],
    );
  }

  Widget buildDivider() {
    return const Divider(
      color: Colors.transparent,
    );
  }

  Widget buildBodyWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 100,
          // color: Colors.amber[600],
          alignment: Alignment.bottomCenter,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              Text(
                'Bagsean, Chonburi',
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Icon(Icons.location_on, color: Colors.white),
            ],
          ),
        ),
        Container(
          height: 200,
          // color: Colors.amber[600],
          alignment: Alignment.center,
          child: const Text(
            "22°C",
            style: TextStyle(
              height: 1.5,
              fontSize: 120,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Container(
          height: 30,
          // color: Colors.amber[600],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              Text(
                '29 ',
                style: TextStyle(
                  fontSize: 22,
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                '/ 22 °C',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 30,
          // color: Colors.amber[600],
          child: const Text(
            'Sunny',
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ],
    );
  }

  Widget buildCardWidget() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: Colors.transparent,
      elevation: 1,
      child: SizedBox(
        width: 600,
        height: 130,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                buildtimeLine23(),
                buildtimeLine24(),
                buildtimeLine01(),
                buildtimeLine02(),
                buildtimeLine03(),
                buildtimeLine04(),
                buildtimeLine05(),
                buildtimeLine06(),
                buildtimeLineSunrise(),
                buildtimeLine07(),
                buildtimeLine08(),
                buildtimeLine09(),
                buildtimeLine10(),
                buildtimeLine11(),
                buildtimeLine12(),
                buildtimeLine13(),
                buildtimeLine14(),
                buildtimeLine15(),
                buildtimeLine16(),
                buildtimeLine17(),
                buildtimeLine18(),
                buildtimeLine19(),
                buildtimeLine20(),
                buildtimeLine21(),
                buildtimeLine22(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildExtendBoradcast() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
      color: Colors.transparent,
      elevation: 1,
      child: SizedBox(
        width: 600,
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Icon(
              Icons.calendar_month,
              color: Colors.white,
            ),
            Text(
              'EXTEND FORECAST',
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
                // fontWeight: FontWeight.bold,
              ),
            ),
            Icon(
              Icons.arrow_right,
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHelpfulInfo() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      color: Colors.transparent,
      elevation: 1,
      child: SizedBox(
        width: 600,
        height: 200,
        child: GridView.count(crossAxisCount: 4, children: <Widget>[
          buildHumidity(),
          buildFeelsLike(),
          buildWindSpeed(),
          buildWindDirection(),
          buildUV(),
        ]),
      ),
    );
  }

  Widget buildHumidity() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 19, bottom: 0),
          child: Icon(Icons.water_drop_outlined, color: Colors.white, size: 35),
        ),
        Text(
          'Humidity',
          style: TextStyle(
            fontSize: 12,
            color: Colors.grey,
            fontWeight: FontWeight.w400,
          ),
        ),
        Text(
          '49%',
          style: TextStyle(
            fontSize: 14,
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }

  Widget buildFeelsLike() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 19, bottom: 0),
          child:
              Icon(CupertinoIcons.thermometer, color: Colors.white, size: 35),
        ),
        Text(
          'Feels like',
          style: TextStyle(
            fontSize: 12,
            color: Colors.grey,
            fontWeight: FontWeight.w400,
          ),
        ),
        Text(
          '23°C',
          style: TextStyle(
            fontSize: 14,
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }

  Widget buildWindSpeed() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 19, bottom: 0),
          child: Icon(CupertinoIcons.wind, color: Colors.white, size: 35),
        ),
        Text(
          'Wind speed',
          style: TextStyle(
            fontSize: 12,
            color: Colors.grey,
            fontWeight: FontWeight.w400,
          ),
        ),
        Text(
          '5-12 km/h',
          style: TextStyle(
            fontSize: 14,
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }

  Widget buildWindDirection() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 19, bottom: 0),
          child: Icon(CupertinoIcons.compass, color: Colors.white, size: 35),
        ),
        Text(
          'Wind direction',
          style: TextStyle(
            fontSize: 12,
            color: Colors.grey,
            fontWeight: FontWeight.w400,
          ),
        ),
        Text(
          'North-east',
          style: TextStyle(
            fontSize: 14,
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }

  Widget buildUV() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 19, bottom: 0),
          child: Text(
            'UV',
            style: TextStyle(
              fontSize: 25,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Text(
          'UV index',
          style: TextStyle(
            fontSize: 12,
            color: Colors.grey,
            fontWeight: FontWeight.w400,
          ),
        ),
        Text(
          'Low',
          style: TextStyle(
            fontSize: 14,
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }

  Widget buildtimeLine23() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '23:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '22°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine24() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '00:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '21°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine01() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '01:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '20°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine02() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '02:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '19°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine03() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '03:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '19°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine04() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '04:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '18°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine05() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '05:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '19°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine06() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '06:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            CupertinoIcons.moon_stars_fill,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '20°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLineSunrise() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '06:34',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 0,
          ),
          child: Icon(
            Icons.arrow_upward,
            color: Colors.yellow,
            size: 20,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            bottom: 3,
          ),
          child: Icon(
            Icons.sunny,
            color: Colors.orangeAccent,
            size: 32,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            'Sunrise',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine07() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '07:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '20°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine08() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '08:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '21°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine09() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '09:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '22°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine10() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '10:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '22°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine11() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '11:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '22°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine12() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '12:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '25°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine13() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '13:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '26°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine14() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '014:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '25°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine15() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '15:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '25°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine16() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '16:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '25°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine17() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '17:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '26°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine18() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '18:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '24°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine19() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '19:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '23°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine20() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '20:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '23°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine21() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '21:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '22°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget buildtimeLine22() {
    return Column(
      children: const <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8),
          child: Text(
            '22:00',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: Icon(
            Icons.all_inclusive,
            color: Colors.yellow,
            size: 35,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8,
            bottom: 8,
            left: 8,
            right: 8,
          ),
          child: Text(
            '22°C',
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }
}
